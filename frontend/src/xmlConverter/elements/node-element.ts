import {XmlParserNodeTypes} from "../types";

import {v4 as uuidv4} from "uuid";
import {XmlParserArgumentElement} from "@/xmlConverter";

export class XmlParserNodeElement {
    private readonly _id: string;
    private _name: string;
    private readonly _parent: XmlParserNodeElement | null;
    private _text?: string;
    private _namespace?: string;
    private readonly _children: XmlParserNodeElement[];
    private readonly _attrs: XmlParserArgumentElement[];

    constructor(name: string, parent: XmlParserNodeElement | null, namespace?: string) {
        this._id = uuidv4();
        this._name = name;
        this._parent = parent;
        this._children = [];
        this._attrs = [];
        this._namespace = namespace;
    }

    setName(newName: string): void {
        this._name = newName;
    }

    setNamespace(newNamespace?: string): void {
        this._namespace = newNamespace;
    }

    getNamespace(): string | undefined {
        return this._namespace;
    }

    getAttrs(): XmlParserArgumentElement[] {
        return this._attrs;
    }

    setText(text: string): boolean {
        this._text = text;
        return true;
    }
    getChildren(): XmlParserNodeElement[] {
        return this._children;
    }

    getNodeType(): XmlParserNodeTypes {
        return this._children.length === 0 && this._text ? 'text' : 'Node';
    }

    getText(): string {
        return this._text && this.getNodeType() === "text" ? this._text : "";
    }

    getName(): string {
        return this._name;
    }

    getParent(): XmlParserNodeElement | null {
        return this._parent;
    }
    getId(): string {
        return this._id;
    }
    getChildById(id: string): XmlParserNodeElement | null {
        for (const node of this._children){
            if (node.getId() == id) {
                return node;
            }
        }
        return null;
    }


    addAttr(argument: XmlParserArgumentElement): boolean {
        if (this._attrs.find((attr) => {
            return attr.getName() == argument.getName() && (!argument.getNamespace() || (argument.getNamespace() && attr.getNamespace() == argument.getNamespace()));
        })){
            return false;
        }
        this._attrs.push(argument);
        return true;
    }

    addChildren(childNode: XmlParserNodeElement): boolean {
        for (const child of this._children){
            if (child === childNode || child.getId() === childNode.getId()){
                return false;
            }
        }
        this._children.push(childNode);
        return true;
    }

    changeText(text?: string): boolean {
        if (this.getNodeType() === "text"){
            this._text = text;
            return true;
        }
        return false;
    }

    cleanAttrs(): void {
        for (const attrKey of Object.keys(this._attrs)){
            this._attrs.length = 0;
        }
    }

    cleanChildren(): void {
        this._children.length = 0;
    }

    deleteAttrByName(name: string, namespace?: string): boolean {
        const newAttrs = this._attrs.filter((attr) => {
            return !(attr.getName() == name && (namespace || (namespace && namespace == attr.getNamespace())));
        });
        const res = newAttrs.length == this._attrs.length
        this._attrs.length = 0;
        newAttrs.forEach((attr) => {
            this._attrs.push(attr);
        })
        return res;
    }

    deleteAttr(attribute: XmlParserArgumentElement): boolean {
        const newAttrs = this._attrs.filter((attr) => {
            return attr != attribute;
        });
        const res = newAttrs.length == this._attrs.length
        this._attrs.length = 0;
        newAttrs.forEach((attr) => {
            this._attrs.push(attr);
        })
        return res;
    }

    deleteChildren(childNode: XmlParserNodeElement): boolean {
        const newChildren = this._children.filter((child) => {
            return childNode != child;
        });
        const res = newChildren.length == this._children.length
        this._children.length = 0;
        newChildren.forEach((child) => {
            this._children.push(child);
        })
        return res;
    }

    deleteChildrenById(id: string): boolean {
        for (let childId = 0; childId < this._children.length; childId++) {
            if (this._children[childId]?.getId() === id) {
                this._children.slice(childId, 1);
                return true;
            }
        }
        return false;
    }
}