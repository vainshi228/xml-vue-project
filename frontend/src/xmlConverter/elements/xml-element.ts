import {XmlParserNodeElement} from "./node-element";
import {SupportedVersions} from "../constants";

export class XmlParserXmlElement {
    private _version?: SupportedVersions;
    private _encoding?: string;
    private _content: XmlParserNodeElement | null;

    constructor() {
        this._content = null;
    }

    setVersion(value: SupportedVersions) {
        this._version = value;
    }

    setEncoding(value: string) {
        this._encoding = value;
    }

    getVersion(): SupportedVersions | undefined {
        return this._version;
    }

    getEncoding(): string | undefined {
        return this._encoding;
    }

    getContent(): XmlParserNodeElement | null {
        return this._content;
    }

    setContent(value: XmlParserNodeElement | null) {
        this._content = value;
    }
}