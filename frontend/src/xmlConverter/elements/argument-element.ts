export class XmlParserArgumentElement {
    private _name: string;
    private _namespace: string | undefined;
    private _value: string | undefined;


    constructor(name: string, value?: string, namespace?: string) {
        this._name = name;
        this._namespace = namespace;
        this._value = value;
    }

    getName(): string {
        return this._name;
    }

    getNamespace(): string | undefined {
        return this._namespace;
    }

    getValue(): string | undefined {
        return this._value;
    }

    setNamespace(value?: string): void {
        this._namespace = value;
    }

    setName(value: string): void {
        this._name = value;
    }

    setValue(value?: string): void {
        this._value = value;
    }

}
