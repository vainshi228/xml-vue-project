import {XmlParserArgumentElement, XmlParserNodeElement, XmlParserXmlElement} from "../elements";
import {XmlParserPositionState} from "../types";
import {SUPPORTED_VERSIONS, SupportedVersions} from "../constants";
import NodeElement from "@/components/graphics-templates/NodeElement.vue";

function isLater(later: string): boolean{
    if (later.length > 1) {
        return false
    }
    if (later <= 'z' && later >= 'a') {
        return true;
    } else if (later <= 'Z' && later >= 'A') {
        return true;
    }
    return false;
}

function isLaterOrNumber(later: string): boolean{
    if (later.length > 1) {
        return false
    }
    if (!isLater) {
        if (later <= '9' && later >= '0') {
            return true;
        }
    } else {
        return true;
    }
    return false;
}

function getElementParentString(element: XmlParserNodeElement | null): string {
    let currentElement: XmlParserNodeElement | null = element;
    let returnString = "";

    while (currentElement) {
        returnString = `[${currentElement.getNamespace() ? currentElement.getNamespace() + ":" : ""}${currentElement.getName()}]` + returnString;
        currentElement = currentElement.getParent();
    }
    return returnString;
}

export function parseStringToXml(xmlString: string): XmlParserXmlElement {

    let xmlElement: XmlParserXmlElement | null = null;
    let currentElement: XmlParserNodeElement | null = null;
    let currentPositionState: XmlParserPositionState = XmlParserPositionState.Start;
    let currentName: string = '';
    let currentAttrName: string = '';
    let currentAttrValue: string | null  = null;
    let currentEndTagName: string = '';
    let currentElementText: string | null  = null;
    let currentNamespace: string | undefined = undefined;
    let currentTagName;
    let isXML: boolean = false;

    for (let i = 0; i < xmlString.length; i++){
        const later = xmlString[i];
        switch (currentPositionState) {
            case XmlParserPositionState.Start:
                if (later != '<') {
                    throw new Error("Incorrect xml format")
                }
                currentPositionState = XmlParserPositionState.XmlElement;
                break;
            case XmlParserPositionState.XmlElement:
                if (later != '?') {
                    throw new Error("Xml file should start with <?xml?> tag")
                }
                isXML = true
                currentPositionState = XmlParserPositionState.TagHeader;
                break;
            case XmlParserPositionState.AttrName:
                if (later == ' ' && currentAttrName === ''){
                    break;
                }
                if (later == '=') {
                    currentPositionState = XmlParserPositionState.AttrValue;
                    break;
                }
                if (!isLaterOrNumber(later)){
                    throw new Error(`#${getElementParentString(currentElement) ?? "[rootTag]"} Attribute should contain in name symbols: a-zA-Z0-9`)
                }
                currentAttrName += later;
                break;
            case XmlParserPositionState.AttrValue:
                if (later == '"') {
                    if (!currentAttrValue) {
                        currentAttrValue = ''
                        break;
                    } else {
                        const attr = new XmlParserArgumentElement(currentAttrName, currentAttrValue)
                        currentElement?.addAttr(attr)
                        currentAttrValue = null;
                        currentAttrName = ''
                        currentPositionState = XmlParserPositionState.TagHeader;
                        break;
                    }
                }
                if (!currentAttrValue && currentAttrValue !== ''){
                    throw new Error(`#${getElementParentString(currentElement)} Attribute "${currentAttrName}" should have value`)
                }
                currentAttrValue += later;
                break;
            case XmlParserPositionState.TagBody:
                if (!currentElementText && later === '>') {
                    break;
                }
                if (later === '\n' ||
                    later === '\t' ||
                    (!currentElementText && later === ' ')) {
                    break;
                }
                if (later === '<'){
                    if (currentElementText) {
                        currentElement?.setText(currentElementText);
                    }
                    currentPositionState = XmlParserPositionState.TagHeader;
                    break;
                }
                if (!currentElementText) {
                    currentElementText = '';
                }
                currentElementText += later;
                break;
            case XmlParserPositionState.TagEnd:
                if (later === '>') {
                    currentPositionState = XmlParserPositionState.TagBody;
                    currentElement?.getParent()?.addChildren(currentElement);
                    currentEndTagName = '';
                    currentName = '';
                    currentNamespace = undefined;
                    currentElementText = '';
                    if (!isXML && currentElement && !currentElement.getParent()) {
                        if (!xmlElement?.getContent()) {
                            xmlElement?.setContent(currentElement);
                        } else {
                            throw new Error("There is only one node can be on root")
                        }
                    } else if (currentElement && currentElement.getParent()) {
                        currentElement = currentElement?.getParent();
                    }
                    break;
                }
                if (!isLaterOrNumber(later)){
                    throw new Error(`#${getElementParentString(currentElement)} Closing tag should have laters: a-zA-Z`)
                }
                if (currentEndTagName === currentElement?.getName() && later == ' ') {
                    break;
                }
                currentEndTagName += later;
                if (currentElement?.getNamespace()) {
                    currentTagName = currentElement?.getNamespace() + ':' + currentElement?.getName()
                } else {
                    currentTagName = currentElement!.getName()
                }
                if (currentTagName?.slice(0, currentEndTagName.length) !== currentEndTagName){
                    throw new Error(`#${getElementParentString(currentElement)} Closing tag name should be "${currentTagName}"`)
                }
                break;
            case XmlParserPositionState.TagHeader:
                if (later === '/') {
                    currentPositionState = XmlParserPositionState.TagEnd;
                    break;
                }
                if (later == '>') {
                    currentName = '';
                    currentNamespace = undefined
                    currentPositionState = XmlParserPositionState.TagBody;
                    break;
                }
                if (later == '?' && isXML && currentElement?.getName() == "xml") {
                    xmlElement = new XmlParserXmlElement();
                    if (currentElement?.getAttrs()) {
                        currentElement?.getAttrs().forEach((attr) => {
                            switch (attr.getName()) {
                                case "version":
                                    if (attr.getValue() && SUPPORTED_VERSIONS.includes(attr.getValue() as SupportedVersions)) {
                                        xmlElement!.setVersion(attr.getValue() as SupportedVersions)
                                    }
                                    break;
                                case "encoding":
                                    if (attr.getValue()) {
                                        xmlElement!.setEncoding(attr.getValue() as string);
                                    }
                                    break;
                            }
                        })
                    }
                    currentElement = null;
                    isXML = false;
                    currentPositionState = XmlParserPositionState.TagEnd;
                    break;
                }
                if (later == ' ') {
                    break;
                }
                if (currentElementText) {
                    throw new Error(`#${getElementParentString(currentElement) ?? "[rootTag]"} This is text node and can't have children nodes`);
                }
                if (!isLater(later)){
                    throw new Error(`#${getElementParentString(currentElement) ?? "[rootTag]"} Name of node or name of attribute should start with later: a-zA-Z`)
                }
                if (currentName === '') {
                    currentName = later;
                    currentPositionState = XmlParserPositionState.TagName;
                } else {
                    currentAttrName += later;
                    currentPositionState = XmlParserPositionState.AttrName;
                }
                break;
            case XmlParserPositionState.TagName:
                if (later === ' ') {
                    if (currentName === '') {
                        break;
                    } else {
                        currentElement = new XmlParserNodeElement(currentName, currentElement, currentNamespace);
                        currentPositionState = XmlParserPositionState.TagHeader;
                        break;
                    }
                } else if (later == '>') {
                    currentElement = new XmlParserNodeElement(currentName, currentElement, currentNamespace);
                    currentName = ''
                    currentNamespace = undefined
                    currentPositionState = XmlParserPositionState.TagBody;
                    break;
                } else if (later == '/') {
                    currentElement = new XmlParserNodeElement(currentName, currentElement, currentNamespace);
                    currentName = ''
                    currentNamespace = undefined
                    currentPositionState = XmlParserPositionState.TagEnd;
                    break;
                } else if (later == ':' && currentName) {
                    currentNamespace = currentName;
                    currentName = ''
                    break;
                }
                if (!isLaterOrNumber(later)){
                    throw new Error(`#${getElementParentString(currentElement) ?? "[rootTag]"} Name of node should have symbols: a-zA-Z0-9`)
                }
                currentName += later;
                break;
        }
    }
    return xmlElement!;
}

export function parseXmlDocument(xmlDoc: XmlParserXmlElement, isBeautiful: boolean = false): string {
    const childString = `<?xml${
        xmlDoc.getVersion() ? ' version="' + xmlDoc.getVersion() + '"' : ''}${
        xmlDoc.getEncoding() ? ' encoding="' + xmlDoc.getEncoding() + '"' : ''}?>`;
    const content = xmlDoc.getContent()
    if (content) {
        if (isBeautiful) {
            return childString + '\n' + beautifulXMLtoStringParser(content)
        }
        return childString + parseXmlToString(content)
    } else {
        throw new Error("Xml document should have content");
    }
}

function parseTag(node: XmlParserNodeElement, childString: string, tabs: string = "", endNode: boolean = false) {
    let attrsString = '';
    const elementAttrs = node.getAttrs()
    for (const attr of elementAttrs){
        const attrName = attr.getNamespace() ? attr.getNamespace() + ':' + attr.getName() : attr.getName();
        attrsString += ` ${attrName}="${attr.getValue() ? attr.getValue() : ""}"`
    }
    const tagName = node.getNamespace() ? node.getNamespace() + ':' + node.getName() : node.getName();
    return `${tabs}<${tagName}${attrsString}${
        childString.replaceAll("\n", "").replaceAll("\t", "") != "" 
            ? ">" + childString + (endNode ? "" : tabs) + "</" + tagName 
            : "/"}>`;
}

export function parseXmlToString(node: XmlParserNodeElement): string{
    let childString = '';
    if (node.getNodeType() === 'Node') {
        for (const child of node.getChildren()) {
            childString += parseXmlToString(child)
        }
    } else {
        childString = node.getText();
    }
    return `${parseTag(node, childString)}`
}

export function beautifulXMLtoStringParser(node: XmlParserNodeElement, tabNumber=0): string{
    let childString = '';
    let endNode: boolean = false;
    const tabs = '\t'.repeat(tabNumber);
    if (node.getNodeType() === 'Node') {
        for (const child of node.getChildren()) {
            childString += '\n' + beautifulXMLtoStringParser(child, tabNumber + 1)
        }
        childString += '\n'
    } else {
        childString = node.getText().trim();
        endNode = true
    }
    return `${parseTag(node, childString, tabs, endNode)}`
}