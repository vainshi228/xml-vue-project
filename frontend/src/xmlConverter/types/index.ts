export type XmlParserNodeTypes = "Node"
    | "text"

export enum XmlParserPositionState{
    Start,
    XmlElement,
    TagName,
    TagHeader,
    TagBody,
    TagEnd,
    AttrName,
    AttrValue
}
