export const SUPPORTED_VERSIONS: SupportedVersions[] = ["1.0"]

export type SupportedVersions = "1.0"