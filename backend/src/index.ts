import express from "express";

const app = express()

app.use('/', express.static('./dist', {
    etag: true,
    cacheControl: true,
    lastModified: true,
}))

app.listen(4000, () => {
    console.log("Server was started")
})